# Два участника р1 и р2 участвуют в дуэли.
# Задача: написать ф-цию, которая принимает две строки и возвращает имя игрока, который выстрелил первым.
# Если выстрелили одновременно, то вернуть строку "tie".

# # Вариант написания ф-ции №1
# def who_is_first(p1, p2):
#     s1 = p1.index('B')
#     s2 = p2.index('B')
#     if s1 > s2:
#         winner = 'p2'
#     elif s1 < s2:
#         winner = 'p1'
#     else:
#         winner = 'tie'
#
#     return print(f'Player {winner} is win!')

# Вариант написания ф-ции №2
def who_is_first(p1, p2):
    diff = p1.find('B') - p2.find('B')
    if diff < 0:
        return print('p1')
    elif diff > 0:
        return print('p2')
    else:
        return print('tie')


p1 = str('  Bang!       ')
p2 = str('     Bang!    ')
who_is_first(p1, p2)

p1 = '         Bang!'
p2 = '       Bang!  '
who_is_first(p1, p2)

p1 = '     Bang!    '
p2 = '     Bang!    '
who_is_first(p1, p2)
