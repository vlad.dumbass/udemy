# 1. Вывести лесенкой символ звёздочки по кол-ву строк, заданных пользователем:
# запросить ввод у пользователя кол-ва строк, вывести звёздочки лесенкой.
#
# - Например, пользователь ввёл число 2. Тогда выводим:
# *
# **
#
# - Например, пользователь ввёл число 4. Тогда выводим:
# *
# **
# ***
# ****
# и так далее, смысл должен быть ясен.
# Примечание: символ можно умножать на число, получая строку с дублированным символом
# (но решить можно и без этого)

# ВАРИАНТ №1
k_max_l = 3                                                   # Коэфициент максимального числа строк                                                    #
add_star = 1

numb_of_l = int(input("Введите число строк: "))

if numb_of_l > k_max_l:                                       # Ограничиваю максимальное введенное значение (чтобы не так много звездочек печаталось)
    print('Это число слишком большое, введите число меньше чем ' + str(k_max_l) + ' (включительно)')

else:
    while numb_of_l != 0:
        numb_of_l -= 1
        print(str('*') * add_star)
        add_star += 1



## ВАРИАНТ №2
# numb_of_l = int(input("Введите число строк: "))
#
# for i in range(0, numb_of_l+1):
#     print("*" * i)