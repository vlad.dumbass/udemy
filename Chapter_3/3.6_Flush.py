# Игра в техасский покер
# У игрока есть 7 карт - 2 в руках и 5 на столе
# Нужно узнать есть ли у него комбинация Flush - это когда у игрока есть 5 карт одной масти

# # Вариант №1
# table_cards = ['2_D', 'J_C', '7_D', '8_D', '10_C']
# hand_cards = ['J_D', '3_D']
#
# H_cards = ['2_H', '3_H', '4_H', '5_H', '6_H', '7_H', '8_H', '9_H', '10_H', 'J_H', 'Q_H', 'K_H', 'A_H']  # ♥
# S_cards = ['2_S', '3_S', '4_S', '5_S', '6_S', '7_S', '8_S', '9_S', '10_S', 'J_S', 'Q_S', 'K_S', 'A_S']  # ♠
# C_cards = ['2_C', '3_C', '4_C', '5_C', '6_C', '7_C', '8_C', '9_C', '10_C', 'J_C', 'Q_C', 'K_C', 'A_C']  # ♣
# D_cards = ['2_D', '3_D', '4_D', '5_D', '6_D', '7_D', '8_D', '9_D', '10_D', 'J_D', 'Q_D', 'K_D', 'A_D']  # ♦
#
# compare_H = 0
# compare_S = 0
# compare_C = 0
# compare_D = 0
#
# stop_H = 0
# stop_S = 0
# stop_C = 0
# stop_D = 0
#
# f = 0
#
# cards = table_cards + hand_cards
#
# if f == 0:
#     for c in cards:
#         for H in H_cards:                   # Сравниваем с ♥
#             stop_H += 1
#             if stop_H == 91:                # Если все пары перебраны, приступаем к перебору следующей масти
#                 break
#             elif compare_H == 5:
#                 f += 1
#                 print(f'Flush with ♥')
#                 break
#             elif c == H:
#                 compare_H += 1
#
# if f == 0:
#     for c in cards:
#         for S in S_cards:                   # Сравниваем с ♠
#             stop_S += 1
#             if stop_S == 91:                  # Если все пары перебраны, приступаем к перебору следующей масти
#                 break
#             elif compare_S == 5:
#                 f += 1
#                 print(f'Flush with ♠')
#                 break
#             elif c == S:
#                 compare_S += 1
#
# if f == 0:
#     for c in cards:
#         for C in C_cards:                     # Сравниваем с ♣
#             stop_C += 1
#             if stop_C == 91:                  # Если все пары перебраны, приступаем к перебору следующей масти
#                 break
#             elif compare_C == 5:
#                 f += 1
#                 print(f'Flush with ♣')
#                 break
#             elif c == C:
#                 compare_C += 1
#
# if f == 0:
#     for c in cards:
#         for D in D_cards:                     # Сравниваем с ♦
#             stop_D += 1
#             if stop_D == 91:                  # Если все пары перебраны, то Флеша нет
#                 break
#             elif compare_D == 5:
#                 f += 1
#                 print(f'Flush with ♦')
#                 break
#             elif c == D:
#                 compare_D += 1
#
# if f == 0:
#     print('No Flush!')

# Вариант №2 - код учителя в видеокурсах (изучил, но писал сам)
table_cards = ['2_D', 'J_C', '7_H', '8_D', '10_C']
hand_cards = ['J_D', '3_D']

table_suit = [s[-1] for s in table_cards]
hand_suit = [s[-1] for s in hand_cards]

sum_suites = table_suit + hand_suit

Flush = 0
for suit in 'SDCH':
    if sum_suites.count(suit) >= 5:
        Flush = 1

if Flush == 0:
    print("No Flush!")
else:
    print("Flush!")
