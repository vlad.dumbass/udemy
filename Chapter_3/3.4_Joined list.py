# На вход подаются два списка длинны N.
# Задача: взять из первого списка все нечетные числа,
# из второго чётные и объеденить их в новом списке с названием joined list
# Примечание: можно сделать двумя циклами for, можно одним, можно с помощью list comprehensions.

# # Вариант №1
# list_1 = [1, 8, 6, 9, 3, 4, 6]
# list_2 = [2, 4, 5, 7, 9, 8, 6, 2, 14, 86]
# list_3 = []
#
# for l1 in list_1:
#     if l1 % 2 != 0:
#         list_3.append(l1)
# for l2 in list_2:
#     if l2 % 2 == 0:
#         list_3.append(l2)
# # list_3.sort()
# print(list_3)

# Вариант №2
list_1 = [1, 8, 6, 9, 3, 4, 6]
list_2 = [2, 4, 5, 7, 9, 8, 6, 2, 14, 86]

odds    = [l1 for l1 in list_1 if l1 % 2 != 0]
evens   = [l2 for l2 in list_2 if l2 % 2 == 0]
list_3 = odds + evens
print(list_3)
