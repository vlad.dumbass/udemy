# Классическая игра камень-ножницы-бумага
# Входим в игру - введя "+", выход из игры/из текущего сеанса игры "-".
# Вводим число до скольки побед играем (максимум 100)
# Играем против компа. Он генерит свое, мы ему свое и смотрим кто же победил. Принимаются только такие символы: R/S/P.
# Ведется статистика

import random

confirmation = '+'
refuse = '-'

win_counter = 0
real_counter = 0
PC_win = 0
user_win = 0

lst = ['R', 'S', 'P']                                   # Создание списка для рандомного выбора для ПК
winning_comb = (('R', 'S'), ('S', 'P'), ('P', 'R'))     # Выиграшные комбинации для Игрока


# Вход в игру
print()
in_game = input('Enter the Game R-S-P? ')
while True:
    if in_game == confirmation:
        print()
        print("\t\t\t\tHello and Goodbye! \nLet's start our fight between Human and Machine!!!")
        print()
        win_counter = int(input('До скольки побед играем? '))
        if 1 <= win_counter <= 100:
            print('Играем до ' + str(win_counter) + ' побед!')
            print()
        break
    elif in_game == refuse:
        print(r'exit game((')
        break


# Процесс игры
while in_game == confirmation:                      # Текущий сеанс игры продолжается пока пользователь не введет "-"

    print()
    user_gesture = input('What is gesture? ')       # "Жест пользователя"
    PC_gesture = random.choice(lst)                 # "Жест компьютера"

# Вариант, если Игрок захотел выйти из текущего сеанса игры
    if user_gesture == '-':
        print(r'exit game((')
        break

# Вариант, если показали одинаковый жест
    elif user_gesture == PC_gesture:
        print('Nobody win')

# Вариант, если игрок выиграл
    elif (user_gesture, PC_gesture) in winning_comb:
        user_win += 1
        if user_win == win_counter:
            print('!!!You win!!!')
            print('Игрок = ' + str(user_win) + '\tPC = ' + str(PC_win))
            break
        else:
            print('Игрок = ' + str(user_win) + '\tPC = ' + str(PC_win))

# Вариант, если РС выигнал
    else:
        PC_win += 1
        if PC_win == win_counter:
            print('!!!PC win!!!')
            print('Игрок = ' + str(user_win) + '\tPC = ' + str(PC_win))
            break
        else:
            print('Игрок = ' + str(user_win) + '\tPC = ' + str(PC_win))
