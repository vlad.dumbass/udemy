# Игра в "21" или "Очко"
# Програмист играет роль раздающего, играют 4 игрока.
# Алгоритм игры:
# - всем игрокам раздается по 2 карты
# - удаление розданных карт из колоды
# - определение победителя
# - добор карт. Выбор кому добавлять карту делает программист
# - после каждой взятой карты: 1 она удаляется из колоды; 2 проводится определение победителя
# - если у кого-то больше 21 - это перебор. Запись о переборе будет зафиксированна за игроком
# Разумеется, игра имеет условный характер. Здесь не переданы все правила игры

import random

print()
print(r'\\\Welcome to our casino///')

# Инициализация колоды карт словарём и списком
deck_table = {'6♥': 6, '7♥': 7, '8♥': 8, '9♥': 9, '10♥': 10, 'J♥': 2, 'Q♥': 3, 'K♥': 4, 'A♥': 11,
              '6♠': 6, '7♠': 7, '8♠': 8, '9♠': 9, '10♠': 10, 'J♠': 2, 'Q♠': 3, 'K♠': 4, 'A♠': 11,
              '6♣': 6, '7♣': 7, '8♣': 8, '9♣': 9, '10♣': 10, 'J♣': 2, 'Q♣': 3, 'K♣': 4, 'A♣': 11,
              '6♦': 6, '7♦': 7, '8♦': 8, '9♦': 9, '10♦': 10, 'J♦': 2, 'Q♦': 3, 'K♦': 4, 'A♦': 11}

deck_of_cards = ['6♥', '7♥', '8♥', '9♥', '10♥', 'J♥', 'Q♥', 'K♥', 'A♥',
                 '6♠', '7♠', '8♠', '9♠', '10♠', 'J♠', 'Q♠', 'K♠', 'A♠',
                 '6♣', '7♣', '8♣', '9♣', '10♣', 'J♣', 'Q♣', 'K♣', 'A♣',
                 '6♦', '7♦', '8♦', '9♦', '10♦', 'J♦', 'Q♦', 'K♦', 'A♦']

player1 = []                                 # Инициализация списка игроков
player2 = []
player3 = []
player4 = []

pl1_sum_point = 0                            # Инициализация переменных в которых хранится сумма их очков
pl2_sum_point = 0
pl3_sum_point = 0
pl4_sum_point = 0

ran_index1 = 0                               # Переменная содержащая индекс последнего элемента в списке карт Игрока 1
ran_index2 = 0
ran_index3 = 0
ran_index4 = 0

bust_pl1 = 0                                 # Флаг "перебор карт у игрока"
bust_pl2 = 0
bust_pl3 = 0
bust_pl4 = 0

x = 1

# Раздача по 2 карты игрокам и определение победителя после первой раздачи:
while x > 0:
    player1 = random.sample(deck_of_cards, k=2)
    deck_of_cards.remove(player1[ran_index1])                           # Удаление первой взятой карты из колоды
    ran_index1 += 1
    deck_of_cards.remove(player1[ran_index1])                           # Удаление второй взятой карты из колоды
    ran_index1 += 1
    pl1_sum_point = sum([deck_table[v] for v in player1])
    if pl1_sum_point == 21:
        print("Победил игрок 1! Карты: " + str(player1), " очки: " + str(pl1_sum_point))
        break
    elif pl1_sum_point > 21:
        print("Победил игрок 1: " + str(player1), " два туза - золотое очко!")
        break
    else:
        print("Игрок 1 -  карты: " + str(player1), " очки: " + str(pl1_sum_point))

    player2 = random.sample(deck_of_cards, k=2)
    deck_of_cards.remove(player2[ran_index2])
    ran_index2 += 1
    deck_of_cards.remove(player2[ran_index2])
    ran_index2 += 1
    pl2_sum_point = sum([deck_table[v] for v in player2])
    if pl2_sum_point == 21:
        print("Победил игрок 2! Карты: " + str(player2), " очки: " + str(pl2_sum_point))
        break
    elif pl2_sum_point > 21:
        print("Победил игрок 2: " + str(player2), " два туза - золотое очко!")
        break
    else:
        print("Игрок 2 -  карты: " + str(player2), " очки: " + str(pl2_sum_point))

    player3 = random.sample(deck_of_cards, k=2)
    deck_of_cards.remove(player3[ran_index3])
    ran_index3 += 1
    deck_of_cards.remove(player3[ran_index3])
    ran_index3 += 1
    pl3_sum_point = sum([deck_table[v] for v in player3])
    if pl3_sum_point == 21:
        print("Победил игрок 3! Карты: " + str(player3), " очки: " + str(pl3_sum_point))
        break
    elif pl3_sum_point > 21:
        print("Победил игрок 3: " + str(player3), " два туза - золотое очко!")
        break
    else:
        print("Игрок 3 -  карты: " + str(player3), " очки: " + str(pl3_sum_point))

    player4 = random.sample(deck_of_cards, k=2)
    deck_of_cards.remove(player4[ran_index4])
    ran_index4 += 1
    deck_of_cards.remove(player4[ran_index4])
    ran_index4 += 1
    pl4_sum_point = sum([deck_table[v] for v in player4])
    if pl4_sum_point == 21:
        print("Победил игрок 4! Карты: " + str(player4), " очки: " + str(pl4_sum_point))
        break
    elif pl4_sum_point > 21:
        print("Победил игрок 4: " + str(player4), " два туза - золотое очко!")
        break
    else:
        print("Игрок 4 -  карты: " + str(player4), " очки: " + str(pl4_sum_point))
    x -= 1
#    print(deck_of_cards)
print()

# Добор карт игроками
while True:
    add_card = int(input("Какому игроку добавить карту?"))
    if add_card == 1:
        if pl1_sum_point > 21:
            continue
        else:
            add_p1 = random.sample(deck_of_cards, k=1)                  # Добавляем еще одну карту Игроку 1
            player1.append(add_p1[0])                                   # Сложение списка можно было сделать и так:
#                                                                         player1 = player1 + add_p1
            deck_of_cards.remove(player1[ran_index1])                   # Вычитаем эту карту из колоды
            ran_index1 += 1

            pl1_sum_point = sum([deck_table[v] for v in player1])       # Подсчитываем кол-во очков с учетом добора карт
            if pl1_sum_point == 21:
                print("Победил игрок 1! Карты: " + str(player1), " очки: " + str(pl1_sum_point))
                break
            elif pl1_sum_point > 21:
                print("Игрок 1 проиграл - перебор! Карты: " + str(player1), " очки: " + str(pl1_sum_point))
                bust_pl1 = 1
                if bust_pl2 == 1:
                    print("Игрок 2 проиграл - перебор! Карты: " + str(player2), " очки: " + str(pl2_sum_point))
                else:
                    print("Игрок 2 -  карты: " + str(player2), " очки: " + str(pl2_sum_point))
                if bust_pl3 == 1:
                    print("Игрок 3 проиграл - перебор! Карты: " + str(player3), " очки: " + str(pl3_sum_point))
                else:
                    print("Игрок 3 -  карты: " + str(player3), " очки: " + str(pl3_sum_point))
                if bust_pl4 == 1:
                    print("Игрок 4 проиграл - перебор! Карты: " + str(player4), " очки: " + str(pl4_sum_point))
                else:
                    print("Игрок 4 -  карты: " + str(player4), " очки: " + str(pl4_sum_point))
                print()
            else:
                print("Игрок 1 -  карты: " + str(player1), " очки: " + str(pl1_sum_point))
                if bust_pl2 == 1:
                    print("Игрок 2 проиграл - перебор! Карты: " + str(player2), " очки: " + str(pl2_sum_point))
                else:
                    print("Игрок 2 -  карты: " + str(player2), " очки: " + str(pl2_sum_point))
                if bust_pl3 == 1:
                    print("Игрок 3 проиграл - перебор! Карты: " + str(player3), " очки: " + str(pl3_sum_point))
                else:
                    print("Игрок 3 -  карты: " + str(player3), " очки: " + str(pl3_sum_point))
                if bust_pl4 == 1:
                    print("Игрок 4 проиграл - перебор! Карты: " + str(player4), " очки: " + str(pl4_sum_point))
                else:
                    print("Игрок 4 -  карты: " + str(player4), " очки: " + str(pl4_sum_point))
                print()

    if add_card == 2:
        if pl2_sum_point > 21:
            continue
        else:
            add_p2 = random.sample(deck_of_cards, k=1)                  # Добавляем еще одну карту Игроку 2
            player2.append(add_p2[0])

            deck_of_cards.remove(player2[ran_index2])                   # Вычитаем эту карту из колоды
            ran_index2 += 1

            pl2_sum_point = sum([deck_table[v] for v in player2])       # Подсчитываем кол-во очков с учетом добора карт
            if pl2_sum_point == 21:
                print("Победил игрок 2! Карты: " + str(player2), " очки: " + str(pl2_sum_point))
                break
            elif pl2_sum_point > 21:
                if bust_pl1 == 1:
                    print("Игрок 1 проиграл - перебор! Карты: " + str(player1), " очки: " + str(pl1_sum_point))
                else:
                    print("Игрок 1 -  карты: " + str(player1), " очки: " + str(pl1_sum_point))
                print("Игрок 2 проиграл - перебор! Карты: " + str(player2), " очки: " + str(pl2_sum_point))
                bust_pl2 = 1
                if bust_pl3 == 1:
                    print("Игрок 3 проиграл - перебор! Карты: " + str(player3), " очки: " + str(pl3_sum_point))
                else:
                    print("Игрок 3 -  карты: " + str(player3), " очки: " + str(pl3_sum_point))
                if bust_pl4 == 1:
                    print("Игрок 4 проиграл - перебор! Карты: " + str(player4), " очки: " + str(pl4_sum_point))
                else:
                    print("Игрок 4 -  карты: " + str(player4), " очки: " + str(pl4_sum_point))
                print()
            else:
                if bust_pl1 == 1:
                    print("Игрок 1 проиграл - перебор! Карты: " + str(player1), " очки: " + str(pl1_sum_point))
                else:
                    print("Игрок 1 -  карты: " + str(player1), " очки: " + str(pl1_sum_point))
                print("Игрок 2 -  карты: " + str(player2), " очки: " + str(pl2_sum_point))
                if bust_pl3 == 1:
                    print("Игрок 3 проиграл - перебор! Карты: " + str(player3), " очки: " + str(pl3_sum_point))
                else:
                    print("Игрок 3 -  карты: " + str(player3), " очки: " + str(pl3_sum_point))
                if bust_pl4 == 1:
                    print("Игрок 4 проиграл - перебор! Карты: " + str(player4), " очки: " + str(pl4_sum_point))
                else:
                    print("Игрок 4 -  карты: " + str(player4), " очки: " + str(pl4_sum_point))
                print()

    if add_card == 3:
        if pl3_sum_point > 21:
            continue
        else:
            add_p3 = random.sample(deck_of_cards, k=1)                  # Добавляем еще одну карту Игроку 3
            player3.append(add_p3[0])

            deck_of_cards.remove(player3[ran_index3])                   # Вычитаем эту карту из колоды
            ran_index3 += 1

            pl3_sum_point = sum([deck_table[v] for v in player3])       # Подсчитываем кол-во очков с учетом добора карт
            if pl3_sum_point == 21:
                print("Победил игрок 3! Карты: " + str(player3), " очки: " + str(pl3_sum_point))
                break
            elif pl3_sum_point > 21:
                if bust_pl1 == 1:
                    print("Игрок 1 проиграл - перебор! Карты: " + str(player1), " очки: " + str(pl1_sum_point))
                else:
                    print("Игрок 1 -  карты: " + str(player1), " очки: " + str(pl1_sum_point))
                if bust_pl2 == 1:
                    print("Игрок 2 проиграл - перебор! Карты: " + str(player2), " очки: " + str(pl2_sum_point))
                else:
                    print("Игрок 2 -  карты: " + str(player2), " очки: " + str(pl2_sum_point))
                print("Игрок 3 проиграл - перебор! Карты: " + str(player3), " очки: " + str(pl3_sum_point))
                bust_pl3 = 1
                if bust_pl4 == 1:
                    print("Игрок 4 проиграл - перебор! Карты: " + str(player4), " очки: " + str(pl4_sum_point))
                else:
                    print("Игрок 4 -  карты: " + str(player4), " очки: " + str(pl4_sum_point))
                print()
            else:
                if bust_pl1 == 1:
                    print("Игрок 1 проиграл - перебор! Карты: " + str(player1), " очки: " + str(pl1_sum_point))
                else:
                    print("Игрок 1 -  карты: " + str(player1), " очки: " + str(pl1_sum_point))
                if bust_pl2 == 1:
                    print("Игрок 2 проиграл - перебор! Карты: " + str(player2), " очки: " + str(pl2_sum_point))
                else:
                    print("Игрок 2 -  карты: " + str(player2), " очки: " + str(pl2_sum_point))
                print("Игрок 3 -  карты: " + str(player3), " очки: " + str(pl3_sum_point))
                if bust_pl4 == 1:
                    print("Игрок 4 проиграл - перебор! Карты: " + str(player4), " очки: " + str(pl4_sum_point))
                else:
                    print("Игрок 4 -  карты: " + str(player4), " очки: " + str(pl4_sum_point))
                print()

    if add_card == 4:
        if pl4_sum_point > 21:
            continue
        else:
            add_p4 = random.sample(deck_of_cards, k=1)                  # Добавляем еще одну карту Игроку 4
            player4.append(add_p4[0])

            deck_of_cards.remove(player4[ran_index4])                   # Вычитаем эту карту из колоды
            ran_index4 += 1

            pl4_sum_point = sum([deck_table[v] for v in player4])       # Подсчитываем кол-во очков с учетом добора карт
            if pl4_sum_point == 21:
                print("Победил игрок 4! Карты: " + str(player4), " очки: " + str(pl4_sum_point))
                break
            elif pl4_sum_point > 21:
                if bust_pl1 == 1:
                    print("Игрок 1 проиграл - перебор! Карты: " + str(player1), " очки: " + str(pl1_sum_point))
                else:
                    print("Игрок 1 -  карты: " + str(player1), " очки: " + str(pl1_sum_point))
                if bust_pl2 == 1:
                    print("Игрок 2 проиграл - перебор! Карты: " + str(player2), " очки: " + str(pl2_sum_point))
                else:
                    print("Игрок 2 -  карты: " + str(player2), " очки: " + str(pl2_sum_point))
                if bust_pl3 == 1:
                    print("Игрок 3 проиграл - перебор! Карты: " + str(player3), " очки: " + str(pl3_sum_point))
                else:
                    print("Игрок 3 -  карты: " + str(player3), " очки: " + str(pl3_sum_point))
                print("Игрок 4 проиграл - перебор! Карты: " + str(player4), " очки: " + str(pl4_sum_point))
                bust_pl4 = 1
                print()
            else:
                if bust_pl1 == 1:
                    print("Игрок 1 проиграл - перебор! Карты: " + str(player1), " очки: " + str(pl1_sum_point))
                else:
                    print("Игрок 1 -  карты: " + str(player1), " очки: " + str(pl1_sum_point))
                if bust_pl2 == 1:
                    print("Игрок 2 проиграл - перебор! Карты: " + str(player2), " очки: " + str(pl2_sum_point))
                else:
                    print("Игрок 2 -  карты: " + str(player2), " очки: " + str(pl2_sum_point))
                if bust_pl3 == 1:
                    print("Игрок 3 проиграл - перебор! Карты: " + str(player3), " очки: " + str(pl3_sum_point))
                else:
                    print("Игрок 3 -  карты: " + str(player3), " очки: " + str(pl3_sum_point))
                print("Игрок 4 -  карты: " + str(player4), " очки: " + str(pl4_sum_point))
                print()
    if add_card == 5:                               # Завершить игру
        break
